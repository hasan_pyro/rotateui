package com.example.hasan.cardswipeview.cardstackview.internal;

import android.view.animation.Interpolator;

import com.example.hasan.cardswipeview.cardstackview.Direction;


public interface AnimationSetting {
    Direction getDirection();

    int getDuration();

    Interpolator getInterpolator();

}

