package com.example.hasan.cardswipeview.cardstackview;

public enum StackFrom {
    None,
    Top,
    Bottom,
    Left,
    Right,
    Rotate
}
