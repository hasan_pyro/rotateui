package com.example.hasan.cardswipeview.cardstackview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hasan.cardswipeview.R;

public class CardStackAdapter extends RecyclerView.Adapter<CardStackAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private int[] colors;
    Context context;

    public CardStackAdapter(Context context, int[] spots) {
        this.inflater = LayoutInflater.from(context);
        this.colors = spots;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,colors[position]));
    }

    @Override
    public int getItemCount() {
        return colors.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;

        ViewHolder(View view) {
            super(view);
            this.cardView = view.findViewById(R.id.cardView);

        }
    }

}

