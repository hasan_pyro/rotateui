package com.example.hasan.cardswipeview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.example.hasan.cardswipeview.cardstackview.CardStackAdapter;
import com.example.hasan.cardswipeview.cardstackview.CardStackLayoutManager;
import com.example.hasan.cardswipeview.cardstackview.CardStackListener;
import com.example.hasan.cardswipeview.cardstackview.CardStackView;
import com.example.hasan.cardswipeview.cardstackview.Direction;
import com.example.hasan.cardswipeview.cardstackview.RewindAnimationSetting;
import com.example.hasan.cardswipeview.cardstackview.StackFrom;
import com.example.hasan.cardswipeview.cardstackview.SwipeAnimationSetting;

public class MainActivity extends AppCompatActivity implements CardStackListener {

    private CardStackLayoutManager manager;
    private CardStackAdapter adapter;
    private CardStackView cardStackView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupCardStackView();
        setupButton();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                refresh();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCardDragging(Direction direction, float ratio) {
        Log.d("CardStackView", "onCardDragging: d = " + direction.name() + ", r = " + ratio);
    }

    @Override
    public void onCardSwiped(Direction direction) {
        Log.d("CardStackView", "onCardSwiped: p = " + manager.getTopPosition() + ", d = " + direction);
    }

    @Override
    public void onCardRewound() {
        Log.d("CardStackView", "onCardRewound: " + manager.getTopPosition());
    }

    @Override
    public void onCardCanceled() {
        Log.d("CardStackView", "onCardCanceled:" + manager.getTopPosition());
    }

    private void setupCardStackView() {
        refresh();
    }

    private void setupButton() {
        View skip = findViewById(R.id.skip_button);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Left)
                        .setDuration(200)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                manager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
            }
        });

        View rewind = findViewById(R.id.rewind_button);
        rewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RewindAnimationSetting setting = new RewindAnimationSetting.Builder()
                        .setDirection(Direction.Bottom)
                        .setDuration(200)
                        .setInterpolator(new DecelerateInterpolator())
                        .build();
                manager.setRewindAnimationSetting(setting);
                cardStackView.rewind();
            }
        });

        View like = findViewById(R.id.like_button);
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Right)
                        .setDuration(200)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                manager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
            }
        });
    }

    private void refresh() {
        manager = new CardStackLayoutManager(getApplicationContext(), this);
        manager.setDirections(Direction.HORIZONTAL);
        manager.setStackFrom(StackFrom.None);
        manager.setStackFrom(StackFrom.Rotate);
        manager.setVisibleCount(2);
        manager.setTranslationInterval(8.0f);
        manager.setScaleInterval(0.95f);
        manager.setSwipeThreshold(0.3f);
        manager.setMaxDegree(10.0f);
        manager.setCanScrollHorizontal(true);
        manager.setCanScrollVertical(false);

        int[] colors = new int[9];
        colors[0] = R.color.colorPrimary;
        colors[1] = R.color.colorPrimaryDark;
        colors[2] = R.color.colorAccent;
        colors[3] = R.color.text_disable_2;
        colors[4] = R.color.text_color_gem;
        colors[5] = R.color.text_color_choices;
        colors[6] = R.color.text_color_paste;
        colors[7] = R.color.text_color_choices;
        colors[8] = R.color.blue;


        adapter = new CardStackAdapter(this, colors);
        cardStackView = findViewById(R.id.card_stack_view);
        cardStackView.setLayoutManager(manager);
        cardStackView.setAdapter(adapter);
    }



}

